﻿using System;

namespace ChangeCounter
{
    class Program
    {
        static float inputVal;

        //Array of all of the pound values
        static int[] coinValues = new int[] { 200, 100, 50, 20, 10, 5, 2, 1 };

        static void Main(string[] args)
        {
           
            getUserInput();

            int totalCoins = 0;

            for(int i=0; i<coinValues.Length; i++)
                totalCoins = totalCoins+checkVal(coinValues[i]);

            Console.WriteLine("Total number of coins: "+totalCoins);
            Console.ReadLine();
        }

        //Gets and validates the user input
        public static void getUserInput()
        {
            
            bool isFloat = false;
            string inputString;
            //Checks whether the input is a float or not
            while (!isFloat)
            {
                Console.WriteLine("Please enter a valid pound value:");
                inputString = Console.ReadLine();
                    inputString = inputString.Replace("£"," ");
                isFloat = float.TryParse(inputString, out inputVal);
                
            }

            inputVal = inputVal * 100;

        }

        /*Checks how many of the specific coin can fit into the input float
        / Puts all of the values into the correct format to be printed
        / Prints the values given and returns the number of coins
        */
        public static int checkVal(int coinVal)
        {
            int numOfCoin = 0;

            //Checks how many times the coin can fit into the total
            while (coinVal <= inputVal)
            {
                inputVal = inputVal - coinVal;
                numOfCoin = numOfCoin + 1;
            }

            //Prints out the number of coins in the correct format
            if (coinVal >= 100)
            {
                coinVal = coinVal / 100;
                Console.WriteLine("£"+coinVal+": "+numOfCoin);
            }else Console.WriteLine(coinVal + "p: " + numOfCoin);
            
            return numOfCoin;
        }
    }
}
